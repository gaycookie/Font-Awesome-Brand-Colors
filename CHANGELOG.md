# Changelog

## 12-12-2020
After a long year, I've decided to update this repository.  
Also I noticed I missed a lot of icons since I created this, so I added them now.

Files can now be found in the build artifacts and on 

### Added
- Sass stylesheet
- Added 'CHANGELOG.md'
- New icons:
  - accessible-icon
  - cloudflare
  - dailymotion
  - deezer
  - edge-legacy
  - firefox-browser
  - google-pay
  - guilded
  - hive
  - ideal
  - innosoft
  - instagram-sqaure
  - instalod
  - microblog
  - mixer
  - octopus-deploy
  - perbyte
  - rust
  - shopify
  - tiktok
  - uncharted
  - unity
  - unsplash
  - watchman-monitoring
  - wodu

### Removed
- CSS Files from the Root
- Removed icons:
  - adobe